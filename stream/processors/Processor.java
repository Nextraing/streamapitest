package stream.processors;

import java.util.Objects;

public class Processor {
    String brand;
    String model;
    Double baseClockFrequency;
    Double maxClockFrequency;

    public Processor(){

    }

    public Processor(String brand, String model, Double maxClockFrequency, Double baseClockFrequency) {
        this.brand = brand;
        this.model = model;
        this.baseClockFrequency = baseClockFrequency;
        this.maxClockFrequency = maxClockFrequency;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Double getBaseClockFrequency() {
        return baseClockFrequency;
    }

    public void setBaseClockFrequency(Double baseClockFrequency) {
        this.baseClockFrequency = baseClockFrequency;
    }

    public Double getMaxClockFrequency() {
        return maxClockFrequency;
    }

    public void setMaxClockFrequency(Double maxClockFrequency) {
        this.maxClockFrequency = maxClockFrequency;
    }

    @Override
    public String toString() {
        return "Processor {" +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", maxClockFrequency=" + maxClockFrequency +
                ", baseClockFrequency=" + baseClockFrequency +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Processor)) return false;
        Processor processor = (Processor) o;
        return Objects.equals(getBrand(), processor.getBrand()) &&
                getModel().equals(processor.getModel()) &&
                Objects.equals(getBaseClockFrequency(), processor.getBaseClockFrequency()) &&
                Objects.equals(getMaxClockFrequency(), processor.getMaxClockFrequency());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBrand(), getModel(), getBaseClockFrequency(), getMaxClockFrequency());
    }
}

