package stream.tests;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import stream.processors.Processor;

import java.util.*;
import java.util.stream.*;

import static org.junit.jupiter.api.Assertions.*;

class ProcessorTest {
    static List<Processor> PROCESSOR_LIST = new ArrayList<>();
    List<Processor> actual;
    List<Processor> expected;

    @BeforeAll
    static void beforeAll() {

        PROCESSOR_LIST.add(new Processor(
                "AMD", "EPYC 7662", 3.3, 2.0));
        PROCESSOR_LIST.add(new Processor(
                "AMD", "EPYC 7F72", 3.7, 3.2));
        PROCESSOR_LIST.add(new Processor(
                "AMD", "Ryzen Threadripper 3970X (BOX)", 4.5, 3.7));
        PROCESSOR_LIST.add(new Processor(
                "AMD", "Ryzen 9 5950X (BOX)", 4.9, 3.4));
        PROCESSOR_LIST.add(new Processor(
                "AMD", "Athlon X4 830", 3.4, 3.0));
        PROCESSOR_LIST.add(new Processor(
                "Intel", "Xeon Gold 6246", 4.2, 3.3));
        PROCESSOR_LIST.add(new Processor(
                "Intel", "Xeon E5-2697 v4", 3.6, 2.3));
        PROCESSOR_LIST.add(new Processor(
                "Intel", "Core i9-9980XE Extreme Edition", 4.5, 3.0));
        PROCESSOR_LIST.add(new Processor(
                "Intel", "Core i5-7400", 3.5, 3.0));
        PROCESSOR_LIST.add(new Processor(
                "Intel", "Core i3-10100F", 4.3, 3.6));
    }

    @BeforeEach
    void setUp() {
        actual = new ArrayList<>();
        expected = new ArrayList<>();
    }

    @Test
    void methodFilterBaseClockFrequencyMoreThanThreeTest() {

        expected.add(new Processor(
                "AMD", "EPYC 7F72", 3.7, 3.2));
        expected.add(new Processor(
                "AMD", "Ryzen Threadripper 3970X (BOX)", 4.5, 3.7));
        expected.add(new Processor(
                "AMD", "Ryzen 9 5950X (BOX)", 4.9, 3.4));
        expected.add(new Processor(
                "Intel", "Xeon Gold 6246", 4.2, 3.3));
        expected.add(new Processor(
                "Intel", "Core i3-10100F", 4.3, 3.6));

        actual = PROCESSOR_LIST.stream()
                .filter(processor -> processor.getBaseClockFrequency() > 3)
                .collect(Collectors.toList());

        assertEquals(expected, actual);
    }

    @Test
    void methodFilterBrandNotEqualsAMDTest() {

        expected.add(new Processor(
                "Intel", "Xeon Gold 6246", 4.2, 3.3));
        expected.add(new Processor(
                "Intel", "Xeon E5-2697 v4", 3.6, 2.3));
        expected.add(new Processor(
                "Intel", "Core i9-9980XE Extreme Edition", 4.5, 3.0));
        expected.add(new Processor(
                "Intel", "Core i5-7400", 3.5, 3.0));
        expected.add(new Processor(
                "Intel", "Core i3-10100F", 4.3, 3.6));

        actual = PROCESSOR_LIST.stream()
                .filter(processor -> !processor.getBrand().equals("AMD"))
                .collect(Collectors.toList());

        assertEquals(expected, actual);
    }

    @Test
    void methodFilterModelBOXTest() {

        expected.add(new Processor(
                "AMD", "Ryzen Threadripper 3970X (BOX)", 4.5, 3.7));
        expected.add(new Processor(
                "AMD", "Ryzen 9 5950X (BOX)", 4.9, 3.4));

        actual = PROCESSOR_LIST.stream()
                .filter(processor -> processor.getModel().contains("BOX"))
                .collect(Collectors.toList());

        assertEquals(expected, actual);
    }

    @Test
    void methodSkipHalfCollectionTest() {

        actual = PROCESSOR_LIST.stream()
                .skip((long) PROCESSOR_LIST.size() / 2)
                .collect(Collectors.toList());

        assertEquals(5, actual.size());
    }

    @Test
    void methodDistinctTest() {
        List<Processor> doubleList = new ArrayList<>();
        doubleList.addAll(PROCESSOR_LIST);
        doubleList.addAll(PROCESSOR_LIST);

        actual = doubleList.stream()
                .distinct()
                .collect(Collectors.toList());

        assertArrayEquals(PROCESSOR_LIST.toArray(), actual.toArray());
    }

    @Test
    void methodMapBrandPlusModelTest() {

        List<String> expectedStringList = new ArrayList<>();

        expectedStringList.add("AMD EPYC 7662");
        expectedStringList.add("AMD EPYC 7F72");
        expectedStringList.add("AMD Ryzen Threadripper 3970X (BOX)");
        expectedStringList.add("AMD Ryzen 9 5950X (BOX)");
        expectedStringList.add("AMD Athlon X4 830");
        expectedStringList.add("Intel Xeon Gold 6246");
        expectedStringList.add("Intel Xeon E5-2697 v4");
        expectedStringList.add("Intel Core i9-9980XE Extreme Edition");
        expectedStringList.add("Intel Core i5-7400");
        expectedStringList.add("Intel Core i3-10100F");

        List<String> actualStringList = PROCESSOR_LIST.stream()
                .map(processor -> processor.getBrand() + " " + processor.getModel())
                .collect(Collectors.toList());

        assertEquals(expectedStringList, actualStringList);
    }

    @Test
    void methodPeekBaseClockFrequencyMultiplyOnTwoTest() {

        expected.add(new Processor(
                "AMD", "EPYC 7662", 3.3, 4.0));
        expected.add(new Processor(
                "AMD", "EPYC 7F72", 3.7, 6.4));
        expected.add(new Processor(
                "AMD", "Ryzen Threadripper 3970X (BOX)", 4.5, 7.4));
        expected.add(new Processor(
                "AMD", "Ryzen 9 5950X (BOX)", 4.9, 6.8));
        expected.add(new Processor(
                "AMD", "Athlon X4 830", 3.4, 6.0));
        expected.add(new Processor(
                "Intel", "Xeon Gold 6246", 4.2, 6.6));
        expected.add(new Processor(
                "Intel", "Xeon E5-2697 v4", 3.6, 4.6));
        expected.add(new Processor(
                "Intel", "Core i9-9980XE Extreme Edition", 4.5, 6.0));
        expected.add(new Processor(
                "Intel", "Core i5-7400", 3.5, 6.0));
        expected.add(new Processor(
                "Intel", "Core i3-10100F", 4.3, 7.2));

        actual = PROCESSOR_LIST.stream()
                .peek(processor -> processor.setBaseClockFrequency(processor.getBaseClockFrequency() * 2))
                .collect(Collectors.toList());

        assertEquals(expected, actual);
    }

    @Test
    void methodLimitExceptLastTest() {

        expected.add(new Processor(
                "AMD", "EPYC 7662", 3.3, 2.0));
        expected.add(new Processor(
                "AMD", "EPYC 7F72", 3.7, 3.2));
        expected.add(new Processor(
                "AMD", "Ryzen Threadripper 3970X (BOX)", 4.5, 3.7));
        expected.add(new Processor(
                "AMD", "Ryzen 9 5950X (BOX)", 4.9, 3.4));
        expected.add(new Processor(
                "AMD", "Athlon X4 830", 3.4, 3.0));
        expected.add(new Processor(
                "Intel", "Xeon Gold 6246", 4.2, 3.3));
        expected.add(new Processor(
                "Intel", "Xeon E5-2697 v4", 3.6, 2.3));
        expected.add(new Processor(
                "Intel", "Core i9-9980XE Extreme Edition", 4.5, 3.0));
        expected.add(new Processor(
                "Intel", "Core i5-7400", 3.5, 3.0));

        actual = PROCESSOR_LIST.stream()
                .limit(PROCESSOR_LIST.size() - 1)
                .collect(Collectors.toList());

        assertEquals(expected, actual);
    }

    @Test
    void methodSortedFirstBaseClockFrequencySecondMaxClockFrequencyTest() {

        expected.add(new Processor(
                "AMD", "EPYC 7662", 3.3, 2.0));
        expected.add(new Processor(
                "Intel", "Xeon E5-2697 v4", 3.6, 2.3));
        expected.add(new Processor(
                "AMD", "Athlon X4 830", 3.4, 3.0));
        expected.add(new Processor(
                "Intel", "Core i5-7400", 3.5, 3.0));
        expected.add(new Processor(
                "Intel", "Core i9-9980XE Extreme Edition", 4.5, 3.0));
        expected.add(new Processor(
                "AMD", "EPYC 7F72", 3.7, 3.2));
        expected.add(new Processor(
                "Intel", "Xeon Gold 6246", 4.2, 3.3));
        expected.add(new Processor(
                "AMD", "Ryzen 9 5950X (BOX)", 4.9, 3.4));
        expected.add(new Processor(
                "Intel", "Core i3-10100F", 4.3, 3.6));
        expected.add(new Processor(
                "AMD", "Ryzen Threadripper 3970X (BOX)", 4.5, 3.7));

        actual = PROCESSOR_LIST.stream()
                .sorted(Comparator.comparingDouble(Processor::getBaseClockFrequency)
                        .thenComparingDouble(Processor::getMaxClockFrequency))
                .collect(Collectors.toList());

        assertEquals(expected, actual);
    }

    @Test
    void methodMapToIntBaseClockFrequencyTest() {

        int[] expectedArray = new int[]{2, 3, 4, 3, 3, 3, 2, 3, 3, 4};

        int[] actualArray = PROCESSOR_LIST.stream()
                .mapToInt(processor -> (int) Math.round(processor.getBaseClockFrequency()))
                .toArray();

        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    void methodMapToDoubleBaseClockFrequencyTest() {

        double[] expectedArray = new double[]{2.0, 3.2, 3.7, 3.4, 3.0, 3.3, 2.3, 3.0, 3.0, 3.6};

        double[] actualArray = PROCESSOR_LIST.stream()
                .mapToDouble(Processor::getBaseClockFrequency)
                .toArray();

        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    void methodMapToLongBaseClockFrequencyTest() {

        long[] expectedArray = new long[]{2, 3, 4, 3, 3, 3, 2, 3, 3, 4};

        long[] actualArray = PROCESSOR_LIST.stream()
                .mapToLong(processor -> Math.round(processor.getBaseClockFrequency()))
                .toArray();

        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    void methodFlatMapTest() {

        List<Object> expectedArray = Arrays
                .asList("[EPYC 7662, AMD, 2.0, 3.3]",
                        "[EPYC 7F72, AMD, 3.2, 3.7]",
                        "[Ryzen Threadripper 3970X (BOX), AMD, 3.7, 4.5]",
                        "[Ryzen 9 5950X (BOX), AMD, 3.4, 4.9]",
                        "[Athlon X4 830, AMD, 3.0, 3.4]",
                        "[Xeon Gold 6246, Intel, 3.3, 4.2]",
                        "[Xeon E5-2697 v4, Intel, 2.3, 3.6]",
                        "[Core i9-9980XE Extreme Edition, Intel, 3.0, 4.5]",
                        "[Core i5-7400, Intel, 3.0, 3.5]",
                        "[Core i3-10100F, Intel, 3.6, 4.3]");

        List<Object> actualArray = PROCESSOR_LIST.stream()
                .flatMap(processor -> Stream.of(Arrays.asList(processor.getModel(),
                        processor.getBrand(),
                        processor.getBaseClockFrequency(),
                        processor.getMaxClockFrequency())))
                .collect(Collectors.toList());

        assertEquals(expectedArray.toString(), actualArray.toString());
    }

    @Test
    void methodFlatMapToIntBaseThenMaxClockFrequencyTest() {

        int[] expectedArray = new int[]{2, 3, 3, 4, 4, 5, 3, 5, 3, 3, 3, 4, 2, 4, 3, 5, 3, 4, 4, 4};

        int[] actualArray = PROCESSOR_LIST.stream()
                .flatMapToInt(processor -> IntStream.of(
                        (int) Math.round(processor.getBaseClockFrequency()),
                        (int) Math.round(processor.getMaxClockFrequency())))
                .toArray();

        assertEquals(Arrays.toString(expectedArray), Arrays.toString(actualArray));
    }

    @Test
    void methodFlapToDoubleBaseThenMaxClockFrequencyTest() {

        double[] expectedArray = new double[]
                {2.0, 3.3, 3.2, 3.7, 3.7, 4.5, 3.4, 4.9, 3.0, 3.4,
                        3.3, 4.2, 2.3, 3.6, 3.0, 4.5, 3.0, 3.5, 3.6, 4.3};

        double[] actualArray = PROCESSOR_LIST.stream()
                .flatMapToDouble(processor -> DoubleStream.of(
                        processor.getBaseClockFrequency(),
                        processor.getMaxClockFrequency()))
                .toArray();

        assertEquals(Arrays.toString(expectedArray), Arrays.toString(actualArray));
    }

    @Test
    void methodFlapToLongBaseThenMaxClockFrequencyTest() {

        long[] expectedArray = new long[]{2, 3, 3, 4, 4, 5, 3, 5, 3, 3, 3, 4, 2, 4, 3, 5, 3, 4, 4, 4};

        long[] actualArray = PROCESSOR_LIST.stream()
                .flatMapToLong(processor -> LongStream.of(
                        Math.round(processor.getBaseClockFrequency()),
                        Math.round(processor.getMaxClockFrequency())))
                .toArray();

        assertEquals(Arrays.toString(expectedArray), Arrays.toString(actualArray));
    }

    @Test
    void methodFindFirstTest() {

        Processor expectedFindFirst = new Processor(
                "AMD", "EPYC 7662", 3.3, 2.0);
        Processor actualFindFirst = PROCESSOR_LIST.stream()
                .findFirst().orElse(new Processor());

        assertEquals(expectedFindFirst, actualFindFirst);
    }

    @Test
    void methodFindAnyOfIntelTest() {

        Processor actualFindAny = PROCESSOR_LIST.stream()
                .filter(processor -> processor.getBrand().equals("Intel"))
                .findAny().orElse(new Processor());

        assertEquals("Intel", actualFindAny.getBrand());
    }

    @Test
    void methodCountIntelProcessorsTest() {

        long expectedCounter = 5;

        long actualCounter = PROCESSOR_LIST.stream()
                .filter(processor -> processor.getBrand().equals("Intel"))
                .count();

        assertEquals(expectedCounter, actualCounter);
    }

    @Test
    void methodCountAllProcessorsTest() {

        long expectedCounter = 10;

        long actualCounter = PROCESSOR_LIST.stream()
                .count();

        assertEquals(expectedCounter, actualCounter);
    }

    @Test
    void methodAnyMatchBOXModelTest() {

        assertTrue(PROCESSOR_LIST.stream()
                .anyMatch(processor -> processor.getModel().contains("BOX")));
    }

    @Test
    void methodNoneMatchBOXModelInIntelBrandTest() {

        assertTrue(PROCESSOR_LIST.stream()
                .filter(processor -> processor.getBrand().equals("Intel"))
                .noneMatch(processor -> processor.getModel().contains("BOX")));
    }

    @Test
    void methodAllMatchDotInBaseClockFrequencyTest() {

        assertTrue(PROCESSOR_LIST.stream()
                .allMatch(processor -> processor.getBaseClockFrequency().toString().contains(".")));
    }

    @Test
    void methodMinOfMaxClockFrequencyTest() {

        Processor expected = new Processor(
                "AMD", "EPYC 7662", 3.3, 2.0);

        Processor actual = PROCESSOR_LIST.stream()
                .min(Comparator.comparingDouble(Processor::getMaxClockFrequency))
                .orElseGet(Processor::new);

        assertEquals(expected, actual);
    }

    @Test
    void methodMaxOfMaxClockFrequencyTest() {

        Processor expected = new Processor(
                "AMD", "Ryzen 9 5950X (BOX)", 4.9, 3.4);

        Processor actual = PROCESSOR_LIST.stream()
                .max(Comparator.comparingDouble(Processor::getMaxClockFrequency))
                .orElseGet(Processor::new);

        assertEquals(expected, actual);
    }

    @Test
    void methodReduceAverageBaseClockFrequencyTest() {

        String expectedReduce = "3,2699";

        double actualReduce = PROCESSOR_LIST.stream()
                .map(Processor::getBaseClockFrequency)
                .reduce((frequencyAverage, processorFrequency) -> ((frequencyAverage + processorFrequency) / 2))
                .orElse(0.0);

        assertEquals(expectedReduce, String.format("%.4f", actualReduce));
    }
}
